FROM openjdk:11.0.4-jre-slim

CMD ["java", "-Duser.country=GB", "-Duser.language=en", "-Duser.timezone=Europe/London", "-XshowSettings:all", "-version"]